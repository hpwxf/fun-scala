// https://stackoverflow.com/questions/6799648/in-scala-what-does-view-do
// (1 to 1000000000).filter(_ % 2 == 0).take(10).toList
// java.lang.OutOfMemoryError: GC overhead limit exceeded

(1 to 1000000000).view.filter(_ % 2 == 0).take(10).toList

//@deprecated("deprecation message", "parallel collections have been removed from scala ≥2.13")
//val x = (1 to 32).par
//// may fail with scala 2.12
//x map { _ * 2 }
