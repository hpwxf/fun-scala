trait Equals[T] {
  def eq(x: T, y: T): Boolean
}

implicit object EqualsInt extends Equals[Int] {
  override def eq(x: Int, y: Int): Boolean = x == y
}

def tcEquals[T : Equals](x: T, y: T): Boolean =
  implicitly[Equals[T]].eq(x,y)

tcEquals(2,3)
