def printSize(countable: { def size: Int }): Unit = {
  print(s"size is ${countable.size}")
}

printSize(1 to 3)

val x = new { def s : Int = 1 }
println(x.s)
