// https://nrinaudo.github.io/2013/08/03/partial-functions.html

val sqrt1 = new PartialFunction[Int, Double] {
  // sqrt is only defined for positive numbers.
  def isDefinedAt(p: Int) = p >= 0

  // Should this be called on a negative number, a MatchError would be thrown.
  def apply(p: Int) = p match {
    case a if a >= 0 => math.sqrt(a.toDouble)
  }
}

// This is strictly equivalent to our previous example, but with much less boilerplate.
// Note that a partial function's type cannot be inferred and needs always be fully declared.
val sqrt: PartialFunction[Int, Double] = {
  case p if p >= 0 => math.sqrt(p.toDouble)
}

sqrt.isDefinedAt(4)
sqrt(4)

try {
  sqrt.isDefinedAt(-4)
  // throw error
  println(sqrt(-4))
} catch {
  case e : Throwable => println(s"sqrt(-4) exception : ${e.getMessage}")
}

// std Function is not aware about its domain definition
val sqrt2 : Function[Int, Double] = {
  case p if p >= 0 => math.sqrt(p.toDouble)
}

try {
  // sqrt2.isDefinedAt(-4) not available
  // throw error
  println(sqrt2(-4))
} catch {
  case e: Throwable => println(s"sqrt2(-4) exception : ${e.getMessage}")
}



/// Collecting values through partial functions
List(4, -4, 9, -9, 16, -16).collect {
  case p if p >= 0 => math.sqrt(p.toDouble)
}

// use a Seq as a filter
List(0, 2, 4).collect(List(4, -4, 9, -9, 16, -16))

/// Lifting
val safeSqrt = sqrt.lift
safeSqrt(4)
safeSqrt(-4)

Function.unlift(safeSqrt).isDefinedAt(-4)


/// Syracuse suite
/// chaining partial functions

// Multiplies all odd values by 3 and add 1.
val step1: PartialFunction[Int, Int] = {
  case p: Int if p % 2 == 1 => 3 * p + 1
}

// Divide by two all even numbers
val step2: PartialFunction[Int, Int] = {
  case p: Int if p % 2 == 0 => p / 2
}

List(1, 2, 3, 4, 5, 6).collect(step1 orElse step2)
