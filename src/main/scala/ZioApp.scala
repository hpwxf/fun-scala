import zio.{ App, ZIO }
import zio.console._
import zio.stream._
import zio._

import scala.io.{ BufferedSource, Source }

object ZioApp extends App {
  override def run(args: List[String]) = {

    // Resource methods
    val acquire: Task[BufferedSource] = ZIO.effect(Source.fromFile("data/data.json"))

    def use(s: BufferedSource) = {
      while (s.hasNext) {
        println(s.next)
      }
      ZIO.unit
    }

    def release(s: BufferedSource) = ZIO.effectTotal(s.close())

    // Resource using Managed (to use later)
    val managedResource = Managed.make(acquire)(release)

    // Reference
    val io_ref = for {
      ref <- Ref.make(100)
      v1  <- ref.get
      _   <- ref.set(v1 - 50)
    } yield ref

    // Stream
    val stream = Stream.fromIterable(1 to 1000)

    val appLogic =
      for {
        ref     <- io_ref
        uio_int = ref.get
        int     <- uio_int
        _       <- putStrLn(s">>> ${int}")

        i1 <- stream.run(Sink.await[Int]) // immutable stream => same call => same result
        i2 <- stream.run(Sink.await[Int]) //
        _  <- putStrLn(s">>> ${i1} - ${i2}")

        _     <- putStrLn("Hello! What is your name?")
        name  <- getStrLn
        _     <- acquire.bracket(release)(use) // only at once
        _     <- managedResource.use { use } // use as many time as you want
        _     <- managedResource.use { use } //
        name2 <- f(name)
        _     <- ZIO.effect(mySideEffect(s"Hello, ${name2}"))
      } yield ()

    appLogic.fold(_ => 1, _ => 0)
  }

  def mySideEffect(s: String): Unit =
    println(s)

  def f(s: String): ZIO[Any, Throwable, String] =
    if (s.isEmpty)
      ZIO.fail(new Throwable)
    else
      ZIO.succeed(s)
}
