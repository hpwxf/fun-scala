// Converting call-by-name parameters to functions
// http://stackoverflow.com/questions/9508051/function-parameter-types-and
// lazy parameter
def toFunction(callByName: => Int): () => Int = callByName _

val a = toFunction(2)
val b = toFunction(5)

println("a=" + a())
println("b=" + b())

def answer = {println("answer"); 40}
def eagerEval(x: Int) = {println("eager"); x+x }
def lazyEval(x: => Int) = {println("lazy"); x+x }

eagerEval(answer + 2)
// > answer
// > eager

lazyEval(answer + 2)
// > lazy
// > answer
