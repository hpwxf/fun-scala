// yield implies a collection result
val s = for( a <- 1 to 3;
             b <- 1 to 3) yield ();

// yield implies a unit result
val s2 = for( a <- 1 to 3;
              b <- 1 to 3) ();


val list : List[Option[Int]] = List(None, Some(1), Some(2))

for {
  l <- list
  x <- l
} yield x

// same as
list.flatMap(
  l => l.map(x => x)
)

// same as
list.flatMap(
  l => l.flatMap(x => Some(x))
)

// NOT same as
list.flatMap(
  l => l.map(x => Some(x))
)
