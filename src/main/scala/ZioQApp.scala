import zio.console._
import zio.stream._
import zio.{ App, ZIO, _ }

import scala.io.{ BufferedSource, Source }

object ZioQApp extends App {
  override def run(args: List[String]) = {

    val fromComposedQueues =
      for {
        q1       <- Queue.bounded[Int](3)
        q2       <- Queue.bounded[Int](3)
        q2Mapped = q2.map(i => (i + 1).toString)
        both     = q1.bothWith(q2Mapped)((_, _))
        _        <- both.offer(1)
        iAndS    <- both.take
        (i, s)   = iAndS
        _        <- putStrLn(s"${i} ${s}")
      } yield ()

    fromComposedQueues.fold(_ => 1, _ => 0)
  }
}
