// More advanced circe: shttp://www.ethanjoachimeldridge.info/tech-blog/circe-beyond-basics

// pour Json
import io.circe.Json
// pour parser
import io.circe.parser
// pour parse()
import io.circe.parser._

// En mode semi-auto, il faut déclarer les implicites
// => compilation bien plus rapide: pas de multiple dérivations
// https://circe.github.io/circe/codecs/semiauto-derivation.html
import io.circe.generic.semiauto.deriveEncoder
import io.circe.generic.semiauto.deriveDecoder

// En mode auto, il n'est pas utile de déclarer les implicit (cela utilise alors shapeless)
// => compilation plus lente: multiple dérivations (à chaque fois qu'un objet est manipulé: pas de factorisation des précédentes dérivations)
// https://circe.github.io/circe/codecs/auto-derivation.html
import io.circe.generic.auto._

// Un Mode manuel est aussi possible mais a priori utile que dans des cas particulier (genre class non case class)
// https://circe.github.io/circe/codecs/custom-codecs.html

// Pour asJson
import io.circe.syntax._

import cats.{ Eq, Show }
import cats.implicits._

import level._
import level.GenericDerivation._

case class Student(name: String, level: Option[Level])

object testCirce {

  def main(args: Array[String]): Unit = {
    testCreate()
    testEncoding()
    testDecoding()
    testOptics()
  }

  def testCreate(): Unit = {
    val jsonFromFields = Json.fromFields(Map(
      "name" -> Json.fromString("John"),
      "id" -> Json.fromInt(42)
    ))

    println(jsonFromFields)
  }


  def testEncoding() = {
    println("** test encoding **")

    implicit val studentEncode = deriveEncoder[Student] // mode semi-auto

    val inputObj = Student(name = "John", level = Some(Intermediate(10)))
    println(inputObj.asJson)

  }

  def testDecoding() = {
    println("** test decoding **")

    implicit val studentDecoder = deriveDecoder[Student] // mode semi-auto

    // this json is not well formatted (cf level tag)
    val inputString =
      """
      [
        {
          "name": "Bob",
          "level": { "i": "10" }
        },
        {
          "name": "Barbara"
        },
        {
          "name": "John",
          "level": { "s": 10 }
        },
        {
          "name": "John",
          "level": { "s": 10 }
        }

      ]
      """.stripMargin
    val decodeResult = parser.decode[List[Student]](inputString)
    decodeResult match {
      case Right(students) => students.map(println)
      case Left(error)     => println(error.show) // .show more convenient than getMessage()
    }
  }

  def testOptics(): Unit = {
    // More examples in https://circe.github.io/circe/optics.html

    println("** test traversing **")

    // pure text parsing (no scheme)
    val result = parse("""
        {
          "order": {
            "customer": {
              "name": "Custy McCustomer",
              "contactDetails": {
                "address": "1 Fake Street, London, England",
                "phone": "0123-456-789"
              }
            },
            "items": [{
              "id": 123,
              "description": "banana",
              "quantity": 1
            }, {
              "id": 456,
              "description": "apple",
              "quantity": 2
            }],
            "total": 123.45
          }
        }
      """)

    result match {
      case Right(json) => {
        println(traverseClassical(json))
        println(traverseOptics(json))
        println(modifyOptics(json))
      }
      case Left(error) => println(error.show) // .show more convenient than getMessage()
    }

    def traverseClassical(json: Json) =
      json.hcursor.downField("order").downField("customer").downField("contactDetails").get[String]("phone").toOption

    def traverseOptics(json: Json) = {
      import io.circe.optics.JsonPath._

      val _phonePath = root.order.customer.contactDetails.phone.as[String]
      _phonePath.getOption(json)
    }

    def modifyOptics(json: Json) = {
      import io.circe.optics.JsonPath._

      val _quantityPath = root.order.items.each.quantity.as[Int]

      val doubleQuantities: Json => Json =
        _quantityPath.modify(_ * 2)

      val json2 = doubleQuantities(json)

      json2.hcursor
        .downField("order")
        .downField("items")
        .focus
    }
  }
}
