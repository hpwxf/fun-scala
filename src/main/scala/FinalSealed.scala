
object testFinalSealed extends App {

  type Id = Int
  type Email = String

  sealed trait Visitor {
      def id: Id
  }

  object Visitor {
    def info(visitor: Visitor): Unit = {
      visitor match {
        case User(id, email) => println("User with id " + id + " and email " + email)
        case Anonymous(_) => println("Anonymous")
        // case _ => println("Undefined visitor") // useless car match complet
      }
    }
  }

  final case class Anonymous(id: Id) extends Visitor
  final case class User(id: Id, var email: Email) extends Visitor

  // var usage : bad practice
  var x: Visitor = User(10, "me@gmail.com")
  Visitor.info(x)
  println(s"${x}")

  x = Anonymous(10)
  Visitor.info(x)

}
