
// Et encore plus ici
// https://github.com/tminglei/slick-pg/blob/master/src/test/scala/com/github/tminglei/slickpg/PgEnumSupportSuite.scala

object ServiceAuthorizations2 {
  class ServiceAuthorization extends Enumeration {
    type ServiceAuthorization = Value
    val ServiceA = Value("Service A")
    val ServiceB = Value("Service B")
    val ServiceC = Value("Service C")
    val Master = Value("Master")
    val Undefined = Value("Undefined")
  }
  object ServiceAuthorization extends ServiceAuthorization

  def ServiceAuthorizationFromString(value: String): ServiceAuthorization.Value = {
    ServiceAuthorization = ServiceAuthorization.withName(value)
  }

  def ServiceAuthorizationToString(auth: ServiceAuthorization): String = {
    auth.toString
  }
}


object ServiceAuthorizations {
  sealed class ServiceAuthorization 
  case object ServiceA extends ServiceAuthorization {
    override def toString: String = "Service A"
  }
  case object ServiceB extends ServiceAuthorization {
    override def toString: String = "Service B"
  }
  case object ServiceC extends ServiceAuthorization {
    override def toString: String = "Service C"
  }
  case object Master extends ServiceAuthorization {
    override def toString: String = "Master"
  }
  case object Undefined extends ServiceAuthorization {
    override def toString: String = "Undefined"
  }

  def ServiceAuthorizationFromString(value: String): ServiceAuthorization = {
    Vector(ServiceA, ServiceB, ServiceC, Master, Undefined).find(_.toString == value).getOrElse(Undefined)
  }

  def ServiceAuthorizationToString(auth: ServiceAuthorization): String = {
    auth.toString
  }

  val commonServices = Seq(ServiceA, ServiceB, ServiceC)
}
