object unapplyTest {
  // example from https://www.journaldev.com/8270/scala-extractors-apply-unapply-and-pattern-matching

  case class Student(name: String, address: Seq[Address])
  case class Address(city: String, state: String)

  object City {
    def unapply(s: Student): Option[Seq[String]] =
      Some(
        for (c <- s.address)
          yield c.state
      )
  }

  class StringSeqContains(value: String) {
    def unapply(in: Seq[String]): Boolean =
      in contains value
  }

  // In the object City we are defining an unapply method which fetches the state from
  // the address list. In main method we are creating objects of Student and address
  // and we are fetching the names of the students who stay in texas city which is
  // the pattern to be matched.

  def main(args: Array[String]) {

    val stud = List(Student("Harris", List(Address("LosAngeles", "California"))),
      Student("Reena", List(Address("Houston", "Texas"))),
      Student("Rob", List(Address("Dallas", "Texas"))),
      Student("Chris", List(Address("Jacksonville", "Florida"))))

    val Texas = new StringSeqContains("Texas")
    val students = stud collect {
      case student @ City(Texas()) => student.name
    }
    println(students)
  }
}
