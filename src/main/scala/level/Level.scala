package level

sealed trait Level
// TODO: Essayer avec un case object
case class Beginner(b: Boolean) extends Level
case class Intermediate(i: Int) extends Level
case class Advanced(s: String) extends Level


