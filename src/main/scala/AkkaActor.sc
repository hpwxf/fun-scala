import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.Await
import scala.concurrent.duration.Duration

println("Starting")

class SimpleActor extends Actor {
  override def receive = {
    case s: String => println("Receive String:" + s)
    case i: Int => println("Receice Int:" + i)
  }

  def foo = println("Foo")
}

val system: ActorSystem = ActorSystem("SimpleSystem", ConfigFactory.load())
val actor = system.actorOf(Props[SimpleActor], "SimpleActor")

actor ! "Hi there"
actor ! 42

implicit val timeout = Timeout(10, TimeUnit.SECONDS)
val r = actor ? 42
Await.result(r, Duration.Inf)
// actor never send answer
