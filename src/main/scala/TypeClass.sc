// http://danielwestheide.com/blog/2013/02/06/the-neophytes-guide-to-scala-part-12-type-classes.html
// https://blog.univalence.io/pourquoi-les-typeclasses-cest-top/

object Math {
  import annotation.implicitNotFound
  @implicitNotFound("No member of type class NumberLike in scope for ${T}")
  trait NumberLike[T] {
    def plus(x: T, y: T): T
    def divide(x: T, y: Int): T
    def minus(x: T, y: T): T
  }
  object NumberLike {
    implicit object NumberLikeDouble extends NumberLike[Double] {
      def plus(x: Double, y: Double): Double = x + y
      def divide(x: Double, y: Int): Double = x / y
      def minus(x: Double, y: Double): Double = x - y
    }
    implicit object NumberLikeInt extends NumberLike[Int] {
      def plus(x: Int, y: Int): Int = x + y
      def divide(x: Int, y: Int): Int = x / y
      def minus(x: Int, y: Int): Int = x - y
    }
  }
}


object Statistics {
  import Math.NumberLike

  def mean[T](xs: Vector[T])(implicit ev: NumberLike[T]): T =
    ev.divide(xs.reduce(ev.plus(_,_)), xs.size)

  def median[T : NumberLike](xs: Vector[T]): T = xs(xs.size / 2)

  def quartiles[T: NumberLike](xs: Vector[T]): (T, T, T) =
    (xs(xs.size / 4), median(xs), xs(xs.size / 4 * 3))

  def iqr[T: NumberLike](xs: Vector[T]): T = quartiles(xs) match {
    case (lowerQuartile, _, upperQuartile) =>
      implicitly[NumberLike[T]].minus(upperQuartile, lowerQuartile)
  }

  /* A context bound T : NumberLike means that an implicit value of type NumberLike[T]
   * must be available, and so is really equivalent to having a second implicit parameter
   * list with a NumberLike[T] in it. If you want to access that implicitly available value,
   * however, you need to call the implicitly method, as we do in the iqr method.
   * If your type class requires more than one type parameter, you cannot use the context
   * bound syntax.
   * https://twitter.github.io/scala_school/advanced-types.html
   * http://docs.scala-lang.org/tutorials/FAQ/context-bounds
   */
}


val numbers = Vector[Double](13, 23.0, 42, 45, 61, 73, 96, 100, 199, 420, 900, 3839)
println(Statistics.mean(numbers))
println(Statistics.median(numbers))
// println(Statistics.iqr(numbers))

val strings = Vector[String]("a","b")
// println(Statistics.mean(strings)) // implicit not found error
