sealed trait Natural {
  def isZero: Boolean
  def predecessor: Natural
  def successor: Natural
  def +(that: Natural): Natural
  def -(that: Natural): Natural
}

case class S(n: Natural) extends Natural {
  override def isZero = false
  override def predecessor = n
  override def successor = S(this)
  override def +(that: Natural) = S(n + that)
  // on ne peut pas utiliser S(n-that) car (n-that) pourrait être négatif
  override def -(that: Natural) = if (that.isZero) this else n - that.predecessor
}

case object Zero extends Natural {
  override def isZero = true
  override def predecessor = throw new Error("0.predecessor")
  override def successor = S(this)
  override def +(that: Natural) = that
  override def -(that: Natural) = if (that.isZero) this else throw new Error("negative number")
}

S(S(S(Zero))) + S(Zero) + Zero - S(S(Zero))

