/*
 * Self-types are a way to declare that a trait must be mixed into another trait, even though it doesn’t directly extend it.
 * That makes the members of the dependency available without imports.
 *
 * A self-type is a way to narrow the type of this or another identifier that aliases this.
 * The syntax looks like normal function syntax but means something entirely different.
 *
 * To use a self-type in a trait, write an identifier, the type of another trait to mix in, and a => (e.g. someIdentifier: SomeOtherTrait =>).
*/

// Un trait
trait User {
  def username: String
}

// Un trait incomplet dépendant de User
trait Tweeter {
  this: User => // reassign this
  def tweet(tweetText: String) = println(s"$username: $tweetText")
}

// Une classe complète avec Tweeter et User
class VerifiedTweeter(val username_ : String) extends Tweeter with User { // We mixin User because Tweeter required it
  def username = s"real ${username_}"
}

// Instanciation
val realBeyoncé = new VerifiedTweeter("Beyoncé")
realBeyoncé.tweet("Just spilled my glass of lemonade") // prints "real Beyoncé: Just spilled my glass of lemonade"

// ***********************

// Un trait
trait Reader {
  def read: String = "data from file"
}

// Un autre trait
trait Writer {
  def write: String = "data written to file"
}

// Un trait étendant le précédent
trait DBWriter extends Writer {
  override def write: String = "data written to database"
}

// Une class ayant besoin des deux premiers traits
class Service {
  this: Reader with Writer =>
  def reading: String = read

  def writing: String = write
}

// Instanciation incomplète
// val service = new Service

// Instanciation valide
val service = new Service with Reader with DBWriter
service.reading
service.writing

// ***********************

trait Function1Option[-A, +B] {
  self =>
  def apply(a: A): Option[B]

  final def andThen[C](that: Function1Option[B, C]): Function1Option[A, C] =
    new Function1Option[A, C] {
      // sans le self type du trait, il n'aurait pas été possible de parler de celui-ci
      // car this est ici associée à la classe en cours de construction et non plus la première
      override def apply(a: A): Option[C] = self.apply(a) match {
        case Some(b) => that.apply(b)
        case None => None
      }
    }
}

