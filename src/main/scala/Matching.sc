import scala.collection.immutable.Seq

val lowerConst = "lower"
val UpperConst = "UPPER"

for (i <- Seq(lowerConst, UpperConst, "should mismatch.").map(Option.apply)) {
  print("Input '%s' results in: ".format(i)) // direct method call from literal
  i match {
    case Some(UpperConst) => println("UPPER!!!") // variable expanded as value
    case Some(`lowerConst`) => println("lower!") // variable expanded as value
    case Some(lowerConst) => println("any Some(_) match!") // local variable match
    case _ => println("mismatch!")
  }
}
