class Equals a where
  eq :: a -> a -> Bool

instance Equals Integer where
  eq x y = x == y

  tcEquals :: (Equals a) => a -> a -> Bool
  tcEquals a b = eq a b

