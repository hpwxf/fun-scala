// https://nrinaudo.github.io/2013/09/08/type-variance.html

// A Mammal has a name (better classes than original article)
sealed trait Mammal {
  def name: String
}

// A Dog is a Mammal that can bark.
// https://stackoverflow.com/questions/34561614/should-i-use-the-final-modifier-when-declaring-case-classes
final case class Dog(name: String) extends Mammal {
  def bark(): Unit = println("bark!")
}

// A Cat is a Mammal that purrs.
final case class Cat(name: String) extends Mammal {
  def purr(): Unit = println("purrrr")
}

/// Invariance
{
  case class Wrapper[A](val a: A) {
    // Retrieves the wrapped value.
    def apply() : A = a
  }

  // Compilation errors below
  // val wd: Wrapper[Dog] = Wrapper(new Mammal("Flipper"))
  // val wm: Wrapper[Mammal] = Wrapper[Dog](new Dog("Lassie"))
}

/// Covariance
{
  case class Wrapper[+A](val a: A) {
    def apply(): A = a
  }

  // Compilation errors below
  // val wd: Wrapper[Dog] = Wrapper(new Mammal("Flipper"))
  // Compilation Ok
  val wm: Wrapper[Mammal] = Wrapper[Dog](new Dog("Lassie"))
}

/// Contravariance
{
  trait Printer[-A] {
    // Prints its argument.
    def apply(a: A): Unit
  }

  // Note how this calls a method defined in Dog but not in Mammal.
  class DogPrinter extends Printer[Dog] {
    override def apply(a: Dog): Unit = {
      println(a)
      a.bark()
    }
  }

  // Since Printer is contravariant, an instance of MammalPrinter is a valid instance of Printer[Dog].
  class MammalPrinter extends Printer[Mammal] {
    override def apply(a: Mammal): Unit = {
      println(a)
    }
  }

  // Compiles: a Printer[Mammal] is a valid Printer[Dog].
  val wd: Printer[Dog] = new MammalPrinter()

  // Compilation fails
  // val wm: Printer[Mammal] = new DogPrinter()

  wd(new Dog("Puppy"))
}

/// Function variance
{
  // Wrapper could not possibly be contravariant on A:
  // it would mean that a Wrapper[Mammal] would be a valid Wrapper[Dog],
  // which would mean that a valid Wrapper[Dog] could return instances of Cat.
  case class Wrapper[+A](val a: A) extends Function0[A] {
    override def apply(): A = a
  }

  // It wouldn’t be possible for Printer to be covariant on A:
  // it would mean that a Printer[Dog] would be a Printer[Mammal],
  // which would allow us to apply Dog specific code to a Mammal - say,
  // call the bark method on an instance of Cat.
  trait Printer[-A] extends Function[A, Unit] {
    def apply(a: A): Unit
  }
}

/// Explanation of the compilation errors

/// Covariant in contravariant position
{
//  case class Wrapper[+A](val a: A) {
//    def apply(): A = a
//
//    // This method will cause a compilation error message.
//    // because set is contravariant in va:A as any Function1[-T, +R]
//    def set(va: A): Wrapper[A] = Wrapper(va)
//  }

  // Fixed using contravariant on result B
  case class Wrapper[+A](val a: A) {
    def apply(): A = a

    // Signature s.t. 'set' is contravariant in va:B (B superclass of A)
    def set[B >: A](va: B): Wrapper[B] = Wrapper(va)
  }
}
// voir article pour plus de commentaires
// https://nrinaudo.github.io/2013/09/08/type-variance.html


