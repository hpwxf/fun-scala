import prime._

object testPrime extends App {
  val start = 1900
  val end = 2100
  println(s"Find all twin prime numbers between $start and $end")
  for (y <- start to end if isPrime(y) && isPrime(y + 2)) println(y)
}
