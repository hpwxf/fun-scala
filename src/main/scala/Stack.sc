class Stack[+T] {
  self /* : Stack[T] */ =>
  def push[S >: T](elem: S): Stack[S] = new Stack[S] {
    override def top: S = elem
    override def pop: Stack[S] = self // or Stack.this but confusing
    override def toString: String =
      elem.toString + " " + self.toString // or Stack.this but confusing
  }
  def top: T = sys.error("no element on stack")
  def pop: Stack[T] = sys.error("no element on stack")
  override def toString: String = ""
}

var s: Stack[Any] = new Stack().push("hello")
s = s.push(new Object())
s = s.push(7)
println(s)


