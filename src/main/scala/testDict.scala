import scala.io._
import java.text.Normalizer

import scala.util.{Failure, Success, Try}

object testDict {
  def main(args: Array[String]) {


    trait MyNormalizer {
      val s: String

      def normalize: String = Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "")
    }

    implicit class MyStringNormalizer(val s: String) extends MyNormalizer
    implicit class MyCharNormalizer(val c: Char) extends MyStringNormalizer(c.toString())

    def scores1(lines: Iterator[String]) = {
      // La normalisation passe tout en String au lieu de simple Char
      val heads = lines.map {
        _.head.normalize.head
      }
      heads.toList.groupBy(identity).mapValues(_.size)
    }

    def scores2(lines: Iterator[String]) = {
      def loop(it: Iterator[Char], map: Map[Char, Int]): Map[Char, Int] = {
        if (it.hasNext) {
          val c = it.next
          val old = map.getOrElse(c, 0)
          loop(it, map - c + (c -> (old + 1)))
        } else {
          map
        }
      }

      val it: Iterator[Char] = lines.map { s => s.head.normalize.head }
      loop(it, Map[Char, Int]())
    }

    val dictPath = "data/francais_full.dict"

    var source: Try[Source] = Try {
      Source.fromFile(dictPath)(Codec.ISO8859)
    }

    def process(source: Source) = {
      val lines = source.getLines().toList
      println("With " + lines.size + " words")

      for (i <- 1 to 5) {
        val start: Long = System.nanoTime()
        if (args.length == 0)
          println(scores1(lines.iterator).toSeq.sortBy(_._2))
        else
          println(scores2(lines.iterator).toSeq.sortBy(_._2))
        val end = System.nanoTime()
        println("Done in " + (end - start) / 1e9 + " seconds")
      }
    }

    source match {
      case Success(s) => process(s)
      case Failure(ex) => println(s"File loading failed : $ex")
    }
  }
}

