// cf
// - https://www.scala-lang.org/api/current/scala/Dynamic.html
// - https://stackoverflow.com/questions/15799811/how-does-type-dynamic-work-and-how-to-use-it
// - https://blog.scalac.io/2015/05/21/dynamic-member-lookup-in-scala.html
import scala.language.dynamics

object DynObj extends Dynamic {
  def applyDynamic(f: String)(i: Int): String = s"method '${f}' called with argument ${i}"
  def selectDynamic(f: String): String = s"attribute '${f}' called"
  def updateDynamic(f: String)(i: Int): String = s"attribute '${f}' called with integer arg '${i}'"
}

DynObj.bar(2)
DynObj.bar
DynObj.bar = 2
