import scala.io._
import scala.xml._
import scala.actors.Actor


val xml = <tag>Hello</tag>

xml match {
  case <tag>{msg}</tag> => println("Tag: " + msg)
  case _ => println("Unknown scheme")
}

/////////////////////


val stocks = Map("AAPL" -> 665, "GOOG" -> 756)

val xml2 =
  <document>
    <header>Stocks info</header>
    <stocks>{createElements}
    </stocks>
  </document>

def createElements() = {
  stocks.map { element =>
    val (ticker, value) = element
    <symbol ticker={ticker}></symbol>
  }
}

val printer = new scala.xml.PrettyPrinter(40, 2)
printer.format(xml2)
println(xml2)

//////////////////////


def getWeatherInfo(woeid: String) = {
  val url = "http://weather.yahooapis.com/forecastrss?w=" + woeid.+("&u=f")

  val response = scala.io.Source.fromURL(url).mkString

  println("HEllo")
  println(scala.io.Source.fromURL(url).mkString)
  println(response)

  val xmlResponse = XML.loadString(response)

  (xmlResponse \\ "location" \\ "@city",
    xmlResponse \\ "location" \\ "@region",
    xmlResponse \\ "condition" \\ "@temp")
}

val start = System.nanoTime()


for(id <- 2502265 to 2502265) {
  println("Info from " + id)
  println(getWeatherInfo(id.toString()))
}
val end = System.nanoTime()
println("Time:" + (end - start)/1e9)



