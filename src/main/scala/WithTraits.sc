abstract class Writer {
  def write(msg: String) : Unit
}

class StringWriter extends Writer {
  val target = new StringBuilder

  def write(msg: String) = target.append(msg)

  override def toString: String = target.toString
}

trait UpperCaseFilter extends Writer {
  abstract override def write(msg: String) = {
    super.write(msg.toUpperCase())
  }
}

trait ProfanityFilter extends Writer {
  abstract override def write(msg: String) = {
    super.write(msg.replace("stupid", "s*****"))
  }
}

def writeStuff(writer: Writer) = {
  writer.write("This is stupid")
  println(writer)
}

writeStuff(new StringWriter)
writeStuff(new StringWriter with UpperCaseFilter)
writeStuff(new StringWriter with ProfanityFilter)
writeStuff(new StringWriter with UpperCaseFilter with ProfanityFilter)
writeStuff(new StringWriter with ProfanityFilter with UpperCaseFilter)

