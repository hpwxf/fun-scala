def f(l : List[Int]) : (List[Int],List[Int]) = {
  type List2 = (List[Int], List[Int])
  val empty : List2 = (List(),List())

  import scala.annotation.tailrec
  @tailrec
  def g(acc: List2, l: List[Int]): List2 = l match {
    case Nil => acc
    case x :: xs =>
      if (x % 2 == 0)
        g((x :: acc._1, acc._2),xs)
      else
        g((acc._1, x :: acc._2),xs)
  }
  g(empty, l)
}

// Split a list of integer into list of even numbers and list of odd numbers
f(List(1,3,4,5,90,2,2,4))
