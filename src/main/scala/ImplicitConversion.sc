import java.util.Calendar

case class  DayShift(shift: Int)

implicit class IntUtils(val number: Int)(implicit s: DayShift = DayShift(0)) {
  def days = this

  def ago = {
    val today = Calendar.getInstance()
    today.add(Calendar.DAY_OF_MONTH, -number - s.shift)
    today.getTime()
  }
}

// old style
// implicit def int2IntUtil(number : Int) = new IntUtils(number)

println(2.days.ago)
println(new IntUtils(2).days)

implicit val x = DayShift(10)
println(2.days.ago)
println(new IntUtils(2).days)
