
/**
 * Classical template function
 * @param elements : set of elements
 * @return elements count
 */
def count1[T](elements: Set[T]): Int = elements.size
count1(Set[Int](1,2,3))

@deprecated("deprecation message", "old forSome syntax")
def count2(elements: Set[T] forSome {type T}): Int = elements.size
count2(Set[Int](1,2,3))

def count3(elements: Set[_]): Int = elements.size
count3(Set[Int](1,2,3))

@deprecated("deprecation message", "view bounds")
def sumOld[T<%Double](elements: Set[T]): Double =
  (0.0 /: elements)((d1,d2) => d1 + d2)
sumOld(Set[Int](1,2,3))
sumOld(Set[Float](1.0f,2.5f))

// Nouvelle syntaxe avec implicit parameters
def sum[T](elements: Set[T])(implicit ev$1: T => Double): Double =
  elements.foldLeft(0.0)((d1,d2) => d1 + d2)
sum(Set[Int](1,2,3))
sum(Set[Float](1.0f,2.5f))
