/**
  * Created by Pascal on 28/12/2016.
  */
object testHello extends App {
  println("Hello world")

  val range = 1 to 10

  def sum(list : List[Int], selector : Int => Boolean = x => true) : Int = list.filter(selector).foldLeft(0) { (c,e) => c + e }


  println("Result1 is " + sum(range.toList, x => x > 5))
  println("Result2 is " + range.map { _ + 1 }.sum)

  // /: right associative operator (ends with a :) for foldLeft (which is left associative for its internal operation)
  //here : ((((((((((0+1)+2)+3)+4)+5)+6)+7)+8)+9)+10)
  // NB: :\ is the left associative operator for foldRight
  println("Result3 is " + (0 /: range) { _ + _ })
  println("Result3 is " + range.foldLeft(0)(_+_))
  println("Result3 is " + range./:(0)(_+_))

  // ??? // Undefined : to do
  val x = (1 until 10).find( _ > 51)
  println("Result4 is " + x.getOrElse(Unit))


  // Lazy variable
  println("------------")
  lazy val lazy_a = { println("lazy a"); 3 }
  val eager_a = { println("eager a"); 3 }
  println("------------")
  println(lazy_a)
  println(eager_a)
  println("------------")
}
