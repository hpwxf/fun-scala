// Examples from https://books.underscore.io/shapeless-guide/shapeless-guide.html

import shapeless._
import shapeless.ops.hlist

case class Employee(name: String, number: Int, manager: Boolean)

case class IceCream(name: String, numCherries: Int, inCone: Boolean)

val genericEmployee = Generic[Employee].to(Employee("Dave", 123, false))
// genericEmployee: String :: Int :: Boolean :: shapeless.HNil = Dave :: 123 :: false :: HNil

val genericIceCream = Generic[IceCream].to(IceCream("Sundae", 1, false))
// genericIceCream: String :: Int :: Boolean :: shapeless.HNil = Sundae :: 1 :: false :: HNil

trait Migration[A, B] {
  def apply(a: A): B
}

implicit class MigrationOps[A](a: A) {

  def migrateTo[B](implicit migration: Migration[A, B]): B =
    migration.apply(a)
}

case class IceCreamV1(name: String, numCherries: Int, inCone: Boolean)

// Remove fields:
case class IceCreamV2a(name: String, inCone: Boolean)

// Reorder fields:
case class IceCreamV2b(name: String, inCone: Boolean, numCherries: Int)

// Insert fields (provided we can determine a default value):
case class IceCreamV2c(name: String, inCone: Boolean, numCherries: Int, numWaffles: Int)

/// Remove fields
  implicit def genericMigration[A, B, ARepr <: HList, BRepr <: HList](
    implicit
    aGen: LabelledGeneric.Aux[A, ARepr],
    bGen: LabelledGeneric.Aux[B, BRepr],
    inter: hlist.Intersection.Aux[ARepr, BRepr, BRepr]
  ): Migration[A, B] = new Migration[A, B] {

    def apply(a: A): B =
      bGen.from(inter.apply(aGen.to(a)))
  }

IceCreamV1("Sundae", 1, false).migrateTo[IceCreamV2a]

/// Reorder fields
implicit def genericMigration[
  A, B,
  ARepr <: HList, BRepr <: HList,
  Unaligned <: HList
](
   implicit
   aGen    : LabelledGeneric.Aux[A, ARepr],
   bGen    : LabelledGeneric.Aux[B, BRepr],
   inter   : hlist.Intersection.Aux[ARepr, BRepr, Unaligned],
   align   : hlist.Align[Unaligned, BRepr]
 ): Migration[A, B] = new Migration[A, B] {
  def apply(a: A): B =
    bGen.from(align.apply(inter.apply(aGen.to(a))))
}

IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2a]
// res8: IceCreamV2a = IceCreamV2a(Sundae,true)

IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2b]
// res9: IceCreamV2b = IceCreamV2b(Sundae,true,1)



// Polys transformation
import scala.math.Numeric

object total extends Poly1 {
  implicit def base[A](implicit num: Numeric[A]):
  Case.Aux[A, Double] =
    at(num.toDouble)

  implicit def option[A](implicit num: Numeric[A]):
  Case.Aux[Option[A], Double] =
    at(opt => opt.map(num.toDouble).getOrElse(0.0))

  implicit def list[A](implicit num: Numeric[A]):
  Case.Aux[List[A], Double] =
    at(list => num.toDouble(list.sum))
}

total(10)
// res15: Double = 10.0

total(Option(20.0))
// res16: Double = 20.0

total(List(1L, 2L, 3L))
// res17: Double = 6.0

