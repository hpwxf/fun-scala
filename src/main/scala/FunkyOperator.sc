case class X(i: Integer) {

  // left to right operator
  def %+%(j: Integer): Integer = {
    i + j + 1
  }

  // right to left operator
  def %+%:(j: Integer): Integer = {
    i + j + 1
  }
}

val x = X(2)

x %+% 2
2 %+%: x
