package prime

object isPrime {
    def apply(n: Int) : Boolean = {
      assert(n >= 2)
      ! (2 +: (3 to Math.sqrt(n).toInt by 2)).exists(n % _ == 0)
    }
  }
// println(isPrime(2017))
