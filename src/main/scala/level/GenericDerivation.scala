package level

import cats.syntax.functor._
import io.circe.{Decoder, Encoder}
import io.circe.generic.auto._
import io.circe.syntax._

object GenericDerivation {
  implicit val encodeEvent: Encoder[Level] = Encoder.instance {
    case x @ Beginner(_) => x.asJson
    case x @ Advanced(_) => x.asJson
    case x @ Intermediate(_) => x.asJson
  }

  implicit val decodeEvent: Decoder[Level] =
    List[Decoder[Level]](
      Decoder[Beginner].widen,
      Decoder[Advanced].widen,
      Decoder[Intermediate].widen,
    ).reduceLeft(_ or _)
}
