
case class Action(title: String) {

  def apply(x : String) : String = {
    println("Eval Action.apply($x)")
    title + " " + x
  }
}


object Main2 extends App {

  def A(s : String)(b: String => String) : Action = {
    println("Eval A($s)")
    Action(b(s))
  }

  println("\n## Initial action")
  var a = Action("title")
  println("Result : " + a("toto"))

  println("\n## action submit1")
  def submit1 = A("a1") { s => s + s }
  var b1 = submit1("arg")
  println("Result : " + b1)

  println("\n## action submit2")
  def f2(x : Action) : Action = {
    println("f2 begin wrapper")
    x
  }
  def submit2 = f2(A("a2") { s => s + s })
  var b2 = submit2("arg")
  println("Result : " + b2)

  println("\n## action submit3")
  def f3(x : (String => String) => Action)(y: String => String) : Action = {
    println("f3 begin wrapper")
    val z = x(y)
    println("f3 end wrapper")
    z
  }
  def submit3 = f3 (A("a3")) { s => s + s }
  var b3 = submit3("arg")
  println("Result : " + b3)

  println("\n## action submit4")
  def f4(x : (String => String) => Action)(y: String => String) : (String => String) = {
    def tmp(a : String) : String = {
      println("f4 begin wrapper")
      val z = x(y)(a)
      println("f4 end wrapper")
      z
    }
    tmp
  }
  def submit4 = f4 (A("a4")) { s => s + s }
  var b4 = submit4("arg")
  println("Result : " + b4)
}