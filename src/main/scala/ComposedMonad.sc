import java.io.IOException

import scala.annotation.tailrec

// Convert List[Either] into Either[List]
  val eithers = List(Left(new IOException("IO error")), Right(2), Right(4), Left(new ArithmeticException("Bad op")))

/// Solution 1
{
    // val lefts = for (Left(x) <- eithers) yield(x) // syntactic sugar of:
    val lefts = eithers.withFilter { case Left(x) => true; case _ => false }.map { case Left(x) => (x) }

    val r1 =
      if (lefts.isEmpty) Right(for (Right(x) <- eithers) yield (x))
      else Left(lefts)
  }

/// Solution 2
{
    val r2 = eithers.partition(_.isLeft) match {
      case (Nil, ints) =>
        Right(for (Right(i) <- ints.view) yield i) // .view produce lazy collection : https://stackoverflow.com/questions/6799648/in-scala-what-does-view-do
      case (strings, _) =>
        Left(for (Left(s) <- strings.view) yield s) // same
    }
  }

/// Solution 3 (not exactly: split List[Either] as 2 lists (lefts,rights)
{
    def splitAsList(l: List[Either[Throwable, Int]]): (List[Throwable], List[Int]) = {
      type List2 = (List[Throwable], List[Int])
      val empty: List2 = (List(), List())

      @tailrec
      def innerRecursion(acc: List2, l: List[Either[Throwable, Int]]): List2 = l match {
        case Nil => acc
        case x :: xs =>
          x match {
            case Right(r) => innerRecursion((acc._1, r :: acc._2), xs)
            case Left(l)  => innerRecursion((l :: acc._1, acc._2), xs)
          }
      }

      innerRecursion(empty, l)
    }

    splitAsList(eithers)
  }

/// Solution 4
{
    // Then you can foldLeft even if initial value is a subtype of 'main' type
    val list = List(None, None, Some(1))
    val r    = list.foldLeft[Option[Int]](None)((acc, e) => Some(1))
    println(r)

    // For
    def mergeEithers[B](
      acc: Either[List[Throwable], List[B]],
      e: Either[Throwable, B]
    ): Either[List[Throwable], List[B]] =
      acc match {
        case Left(l) =>
          e match {
            case Left(y)  => Left(l :+ y)
            case Right(x) => Left(l)
          }
        case Right(r) =>
          e match {
            case Left(y)  => Left(List(y))
            case Right(x) => Right(r :+ x)
          }
      }

    // TODO how to compute return type (to inject into foldLeft to remove ambiguity with initial value)
    // F[ List[Either[Exception, Int]] ] => Either[List[Throwable], List[Int]]
    val r4 =
      eithers.foldLeft[Either[List[Throwable], List[Int]]](Right(List.empty[Int]))((acc, e) => mergeEithers(acc, e))
    println(r4)
  }
