name := "ScalaPower"

version := "1.0"

scalaVersion := "2.12.10"

// add dependencies on standard Scala modules, in a way
// supporting cross-version publishing
// taken from: http://github.com/scala/scala-module-dependency-sample
libraryDependencies := {
  CrossVersion.partialVersion(scalaVersion.value) match {
    // if Scala 2.12+ is used, use scala-swing 2.x
    case Some((2, scalaMajor)) if scalaMajor >= 12 =>
      libraryDependencies.value ++ Seq(
        "org.scala-lang.modules" %% "scala-xml"                % "1.0.6",
        "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
        "org.scala-lang.modules" %% "scala-swing"              % "2.0.0-M2"
      )
    case Some((2, scalaMajor)) if scalaMajor >= 11 =>
      libraryDependencies.value ++ Seq(
        "org.scala-lang.modules" %% "scala-xml"                % "1.0.6",
        "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
        "org.scala-lang.modules" %% "scala-swing"              % "1.0.2"
      )
    case _ =>
      // or just libraryDependencies.value if you don't depend on scala-swing
      libraryDependencies.value :+ "org.scala-lang" % "scala-swing" % scalaVersion.value
  }
}

// libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor"                          % "2.4.16",
  "com.typesafe.akka" %% "akka-agent"                          % "2.4.16",
  "com.typesafe.akka" %% "akka-camel"                          % "2.4.16",
  "com.typesafe.akka" %% "akka-cluster"                        % "2.4.16",
  "com.typesafe.akka" %% "akka-cluster-metrics"                % "2.4.16",
  "com.typesafe.akka" %% "akka-cluster-sharding"               % "2.4.16",
  "com.typesafe.akka" %% "akka-cluster-tools"                  % "2.4.16",
  "com.typesafe.akka" %% "akka-contrib"                        % "2.4.16",
  "com.typesafe.akka" %% "akka-multi-node-testkit"             % "2.4.16",
  "com.typesafe.akka" %% "akka-osgi"                           % "2.4.16",
  "com.typesafe.akka" %% "akka-persistence"                    % "2.4.16",
  "com.typesafe.akka" %% "akka-persistence-tck"                % "2.4.16",
  "com.typesafe.akka" %% "akka-remote"                         % "2.4.16",
  "com.typesafe.akka" %% "akka-slf4j"                          % "2.4.16",
  "com.typesafe.akka" %% "akka-stream"                         % "2.4.16",
  "com.typesafe.akka" %% "akka-stream-testkit"                 % "2.4.16",
  "com.typesafe.akka" %% "akka-testkit"                        % "2.4.16",
  "com.typesafe.akka" %% "akka-distributed-data-experimental"  % "2.4.16",
  "com.typesafe.akka" %% "akka-typed-experimental"             % "2.4.16",
  "com.typesafe.akka" %% "akka-persistence-query-experimental" % "2.4.16"
)

val circeVersion = "0.12.0"
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
  "io.circe" %% "circe-optics"
).map(_ % circeVersion)

libraryDependencies ++= Seq(
  "dev.zio" %% "zio"              % "1.0.0-RC16",
  "dev.zio" %% "zio-interop-cats" % "1.3.1.0-RC3",
  "dev.zio" %% "zio-streams"      % "1.0.0-RC16",
  "dev.zio" %% "zio-delegate"     % "0.0.3"
)

libraryDependencies ++= Seq(
  "com.chuusai" %% "shapeless" % "2.3.3"
)

/** codegen project containing the customized code generator */
lazy val pkgTest = (project in file("pkgTest"))
